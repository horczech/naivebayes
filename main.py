from sklearn import metrics
from path import Path
import json
import numpy as np


def read_data_from_file(readFileName):
    with open(str(readFileName), "r", encoding='UTF-8') as f:
        readedList = json.loads(f.read())
        return readedList

DATASET_PATH = Path('data/Naive_Bayes_data')
SENTENCE_LIST = Path(r'NB_sentence_list.txt')
LABEL_LIST = Path(r'label_list.txt')

sentence_list = read_data_from_file(DATASET_PATH.joinpath(SENTENCE_LIST))
labels = read_data_from_file(DATASET_PATH.joinpath(LABEL_LIST))

# modify the input data
data = [' '.join(sentence) for sentence in sentence_list]

# 0 - negative
# 1 - positive
labels_np = np.asarray(labels, dtype=np.int64)
labels_np[labels_np <= 3] = 0
labels_np[labels_np > 3] = 1

train_data = data[:7000]
train_labels = labels_np[:7000]

test_data = data[7000:]
test_labels = labels_np[7000:]


from sklearn.feature_extraction.text import CountVectorizer
count_vect = CountVectorizer()
X_train_counts = count_vect.fit_transform(train_data)



from sklearn.feature_extraction.text import TfidfTransformer
tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)



from sklearn.naive_bayes import MultinomialNB
clf = MultinomialNB().fit(X_train_tfidf, train_labels)



X_test_counts = count_vect.transform(test_data)
X_test_tfidf = tfidf_transformer.transform(X_test_counts)
predicted = clf.predict(X_test_tfidf)


print(metrics.classification_report(test_labels, predicted))



